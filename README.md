# Proyecto prueba para linets

antes de manipular el codigo se neceita intalar los requerimientos 

    pip install -r requirements.txt

este proyecto tiene como objetivo desarrollar 3 requerimientos

1. Crear un comando personalizado de django que genere el fichero csv
2. Crear un endpoint utilizando rest framework para:
    1. Consultar productos
    2. Cargar nuevos productos
3. Crear una interfaz web para generar el csv

## comando peronalizado

el comando se ejecuta usando **python manage.py csvproductexport > namefile.csv** el cual generara el archivo .csv en la carpeta del pryecto

## endpoint

se generaron 2 endpoint para satisfacer el requerimiento.

1. **<http://localhost/core/api/list/sku>** metodo GET el cual recibe como parametro el nombre del sku a buscar, si no se menciona ningun sku, retorna la lista completa de la BBDD.
2. **<http://localhost/core/api/create/>** metodo POST el, recibe un JSON con los campos a llenar , ejemplo :

{

    "sku": "1994sku",
    "model": "MODELO",
    "group_by_model": "",
    "name": "NOMBRE ",
    "price": "55000",
    "attribute_color": "COLOR ",   
}

## Crear una interfaz web para generar el csv

se genero una interface grafica usando Bootstrap 4  
solo se necesita pinchar el boton y se descargara el archivo csv
