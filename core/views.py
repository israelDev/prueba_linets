import csv 
from django.shortcuts import render
from django.http import HttpResponse
from core.models import CoreMasterproductsconfigurable
from core.serializers import CoreMasterproductsconfigurableSerializer
from rest_framework import mixins
from rest_framework import generics



class ProductList(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  mixins.RetrieveModelMixin,
                  generics.GenericAPIView):
    queryset =  CoreMasterproductsconfigurable.objects.all()
    serializer_class = CoreMasterproductsconfigurableSerializer
    lookup_field = 'sku'
    def get(self, request, sku=None):
        if sku:
            return self.retrieve(request , sku)
        else:
            return self.list(request)
    
class CreateProduct(mixins.CreateModelMixin,
                  generics.GenericAPIView):
    queryset =  CoreMasterproductsconfigurable.objects.all()
    serializer_class = CoreMasterproductsconfigurableSerializer
    def post(self, request):
        return self.create(request)
    
        
def product_export(request):
    response = HttpResponse ( content_type='text/csv')
    writer = csv.writer(response, delimiter = ';')

    writer.writerow(['SKU','name','price','configurable_variatons'])
    
 
    for product in CoreMasterproductsconfigurable.objects.all():
        configurations_variatons = "sku=" + product.sku + " | " + product.attribute_color
        print(product.model , " nombre " , product.name , " configurations_variatons ", configurations_variatons)
        writer.writerow([product.model , product.name , product.price ,  configurations_variatons])   
    response['Content-Disposition'] = 'attachment; filename="product_export.csv"'
    return response


def csv_export():
    print('SKU ; name ; price ; configurable_variatons')
    
    for prodcut in CoreMasterproductsconfigurable.objects.all():
        configurations_variatons = "sku=" + prodcut.sku + " | " + prodcut.attribute_color
        print(prodcut.model , ";" , prodcut.name , ";",  prodcut.price, ";", configurations_variatons)  


def home(request):
    return render(request ,'core/home.html' )


    