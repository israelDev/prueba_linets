from django.core.management.base import BaseCommand, CommandError
from ... import views


class Command(BaseCommand):
    help = """ escriba el comando csvproductexport para ver el csv en consola 
               escriba el comando csvproductexport > namefile.csv para guardarlo como csv desde la carpeta en donde se ejecute el comando"""
    
    def handle(self, *args, **options):
        return views.csv_export()