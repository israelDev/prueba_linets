from django.urls import path

from . import views

urlpatterns = [
    path('', views.home),
    path('export', views.product_export , name="Export"),
    path('api/list/', views.ProductList.as_view()),
    path('api/list/<str:sku>/', views.ProductList.as_view()),
    path('api/create/', views.CreateProduct.as_view())
    
]