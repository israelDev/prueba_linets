from rest_framework import serializers
from .models import CoreMasterproductsconfigurable


class CoreMasterproductsconfigurableSerializer(serializers.ModelSerializer):
    class Meta:
        model = CoreMasterproductsconfigurable
        fields = '__all__'
         